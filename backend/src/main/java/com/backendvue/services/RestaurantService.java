package com.backendvue.services;

import com.backendvue.entities.Restaurant;

import java.sql.SQLException;
import java.util.List;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 7/9/2017
 * Time: 7:08 PM
 */
public interface RestaurantService {

    void saveRestaurant(Restaurant restaurant) throws SQLException;

    List<Restaurant> listAllRestaurants() throws SQLException;

    Restaurant getRestaurantById(long id) throws SQLException;

}
