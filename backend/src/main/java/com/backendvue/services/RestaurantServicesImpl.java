package com.backendvue.services;

import com.backendvue.entities.Restaurant;
import com.backendvue.repositories.RestaurantRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 7/9/2017
 * Time: 7:09 PM
 */
@Service
public class RestaurantServicesImpl implements  RestaurantService {

    private final Log log = LogFactory.getLog(RestaurantServicesImpl.class);

    @Autowired
    private RestaurantRepository restaurantRepository;


    @Override
    public void saveRestaurant(Restaurant restaurant) throws SQLException {
        log.info("restaurant guardado");
        restaurantRepository.save(restaurant);

    }

    @Override
    public List<Restaurant> listAllRestaurants() throws SQLException {
        return restaurantRepository.findAll();
    }

    @Override
    public Restaurant getRestaurantById(long id) throws SQLException {

        return restaurantRepository.findById(id);
    }


}
