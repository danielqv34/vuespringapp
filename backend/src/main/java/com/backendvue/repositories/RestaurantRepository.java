package com.backendvue.repositories;

import com.backendvue.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 7/9/2017
 * Time: 7:03 PM
 */
@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Serializable> {
    Restaurant findById(long id);
}
