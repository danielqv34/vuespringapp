package com.backendvue.entities;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 7/9/2017
 * Time: 6:57 PM
 */
@Entity
public class Restaurant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "NAME", length = 40)
    @NotBlank(message = "EL campo nombre no puede estar en blanco")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ADDRESS", length = 200)
    private String address;

    @Column(name = "IMAGE", length = 1000)
    private String image;

    @Column(name = "PRICE")
    private double price;

    public Restaurant() {
    }

    public Restaurant(String name, String description, String address, String image, double price) {
        this.name = name;
        this.description = description;
        this.address = address;
        this.image = image;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", address='" + address + '\'' +
                ", image='" + image + '\'' +
                ", price=" + price +
                '}';
    }
}
