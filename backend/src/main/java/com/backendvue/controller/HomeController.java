package com.backendvue.controller;

import com.backendvue.entities.Restaurant;
import com.backendvue.services.RestaurantService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

/**
 * Created in IntelliJ IDEA.
 * User: Daniel QuirozV
 * Date: 7/9/2017
 * Time: 6:50 PM
 */
@RestController
public class HomeController {

    private final Log log = LogFactory.getLog(HomeController.class);

    @Autowired
    private RestaurantService restaurantService;

    @GetMapping(value = "/home")
    public String home() {
        log.info("Llamada del Controlador Inicial");
        return "Hola Mundo";
    }

    @GetMapping(value = "/restaurantsList")
    public ResponseEntity<List<Restaurant>> restaurantList() throws SQLException {
        log.info("Llamada para devolver las lista de Restaurantes");
        List<Restaurant> restaurantList = restaurantService.listAllRestaurants();
        if (restaurantList.isEmpty()) {
            log.info("No existen Restaurantes");
            return new ResponseEntity<List<Restaurant>>(restaurantList, HttpStatus.OK);
        } else {
            return new ResponseEntity<List<Restaurant>>(restaurantList, HttpStatus.OK);
        }
    }

    @GetMapping (value = "/getRestaurant/{id}")
    public ResponseEntity<Restaurant> getRestaurantById(@PathVariable(name = "id") long id) throws SQLException {
        Restaurant restaurant = restaurantService.getRestaurantById(id);
        if (restaurant == null) {
            log.info("No existe este auto");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            log.info(restaurant.toString());
            return new ResponseEntity<>(restaurant,HttpStatus.OK);
        }

    }

}
