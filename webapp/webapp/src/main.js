import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Home from './Components/Home.vue'
import Restaurants from './Components/Restaurants.vue'
import RestaurantsTop from './Components/RestaurantsTop.vue'
import Contact from './Components/Contact.vue'
import Restaurant from './Components/Restaurant.vue'



Vue.use(VueRouter);


const routes = [
  { path: '/home', component: Home },
  { path: '/restaurant/:id', name:'restaurant', component:Restaurant},
  { path: '/editRestaurant/:id',name:'editRestaurant', component: Home },
  { path: '/restaurants', component: Restaurants },
  { path: '/topRestaurants/:id',name:'topRestaurants', component: RestaurantsTop },
  { path: '/contacts', component: Contact }
];

const router  = new VueRouter({
  routes,
  mode: 'history'
});

Vue.component('home', Home);
Vue.component('restaunts', Restaurants);
Vue.component('topRestaurants', RestaurantsTop);
Vue.component('contacto', Contact);


new Vue({
  el: '#app',
  router,
  render: h => h(App)
})


